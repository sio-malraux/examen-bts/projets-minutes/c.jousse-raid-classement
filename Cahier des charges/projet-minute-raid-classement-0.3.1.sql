SET client_encoding = 'UTF8';
DROP SCHEMA "raid_classement" CASCADE;
CREATE SCHEMA "raid_classement";

CREATE TABLE "raid_classement"."activite" (
    "code" character varying(5) NOT NULL,
    "libelle" character varying(50)
);
CREATE TABLE "raid_classement"."activite_raid" (
    "code_raid" character varying(6) NOT NULL,
    "code_activite" character varying(5) NOT NULL
);
CREATE TABLE "raid_classement"."coureur" (
    "licence" character varying(5) NOT NULL,
    "nom" character varying(50),
    "prenom" character varying(50),
    "sexe" character(1),
    "nationalite" character varying(50),
    "mail" character varying(50),
    "certif_med_aptitude" boolean DEFAULT false,
    "datenaiss" "date"
);
CREATE TABLE "raid_classement"."departement" (
    "numero" integer NOT NULL,
    "nom" character varying(30)
);
CREATE TABLE "raid_classement"."equipe" (
    "numero" integer NOT NULL,
    "code_raid" character varying(6) NOT NULL,
    "nom" character varying(50),
    "dateinscription" "date" DEFAULT "now"(),
    "temps_global" integer DEFAULT 0,
    "classement" integer DEFAULT 0,
    "penalites_bonif" integer DEFAULT 0
);
CREATE TABLE "raid_classement"."integrer_equipe" (
    "numero_equipe" integer NOT NULL,
    "code_raid" character varying(6) NOT NULL,
    "licence" character varying(5) NOT NULL,
    "temps_individuel" integer DEFAULT 0,
    "num_dossard" character varying(15)
);
CREATE TABLE "raid_classement"."raid" (
    "code" character varying(5) NOT NULL,
    "nom" character varying(50),
    "datedebut" character varying(10),
    "ville" character varying(50),
    "region" character varying(50),
    "nb_maxi_par_equipe" integer,
    "montant_inscription" numeric(8,2),
    "nb_femmes" integer,
    "duree_maxi" integer,
    "age_minimum" integer
);
CREATE TABLE "raid_classement"."traverser" (
    "code_raid" character varying(6) NOT NULL,
    "numero_dept" integer NOT NULL
);
ALTER TABLE ONLY "raid_classement"."activite_raid"
    ADD CONSTRAINT "pk_activite_raid" PRIMARY KEY ("code_raid", "code_activite");

ALTER TABLE ONLY "raid_classement"."departement"
    ADD CONSTRAINT "pk_departement" PRIMARY KEY ("numero");

ALTER TABLE ONLY "raid_classement"."activite"
    ADD CONSTRAINT "pk_activite" PRIMARY KEY ("code");

ALTER TABLE ONLY "raid_classement"."coureur"
    ADD CONSTRAINT "pk_coureur" PRIMARY KEY ("licence");

ALTER TABLE ONLY "raid_classement"."equipe"
    ADD CONSTRAINT "pk_equipe" PRIMARY KEY ("numero", "code_raid");

ALTER TABLE ONLY "raid_classement"."integrer_equipe"
    ADD CONSTRAINT "pk_integrer" PRIMARY KEY ("numero_equipe", "code_raid", "licence");

ALTER TABLE ONLY "raid_classement"."raid"
    ADD CONSTRAINT "pk_raid" PRIMARY KEY ("code");

ALTER TABLE ONLY "raid_classement"."traverser"
    ADD CONSTRAINT "pk_traverser" PRIMARY KEY ("code_raid", "numero_dept");

ALTER TABLE ONLY "raid_classement"."activite_raid"
    ADD CONSTRAINT "activite_raid_code_activite_fkey" FOREIGN KEY ("code_activite") REFERENCES "raid_classement"."activite"("code");

ALTER TABLE ONLY "raid_classement"."activite_raid"
    ADD CONSTRAINT "activite_raid_code_raid_fkey" FOREIGN KEY ("code_raid") REFERENCES "raid_classement"."raid"("code");

ALTER TABLE ONLY "raid_classement"."equipe"
    ADD CONSTRAINT "equipe_code_raid_fkey" FOREIGN KEY ("code_raid") REFERENCES "raid_classement"."raid"("code");

ALTER TABLE ONLY "raid_classement"."integrer_equipe"
    ADD CONSTRAINT "inscrire_individuel_code_coureur_fkey" FOREIGN KEY ("licence") REFERENCES "raid_classement"."coureur"("licence");

ALTER TABLE ONLY "raid_classement"."integrer_equipe"
    ADD CONSTRAINT "inscrire_individuel_equipe_fkey" FOREIGN KEY ("numero_equipe", "code_raid") REFERENCES "raid_classement"."equipe"("numero", "code_raid");

ALTER TABLE ONLY "raid_classement"."traverser"
    ADD CONSTRAINT "traverser_code_dept_fkey" FOREIGN KEY ("numero_dept") REFERENCES "raid_classement"."departement"("numero");

ALTER TABLE ONLY "raid_classement"."traverser"
    ADD CONSTRAINT "traverser_code_raid_fkey" FOREIGN KEY ("code_raid") REFERENCES "raid_classement"."raid"("code");
GRANT USAGE ON SCHEMA "raid_classement" TO "etudiants-slam";
GRANT SELECT ON TABLE "raid_classement"."activite" TO "etudiants-slam";
GRANT SELECT ON TABLE "raid_classement"."activite_raid" TO "etudiants-slam";
GRANT SELECT ON TABLE "raid_classement"."coureur" TO "etudiants-slam";
GRANT SELECT ON TABLE "raid_classement"."departement" TO "etudiants-slam";
GRANT SELECT ON TABLE "raid_classement"."equipe" TO "etudiants-slam";
GRANT SELECT ON TABLE "raid_classement"."integrer_equipe" TO "etudiants-slam";
GRANT SELECT ON TABLE "raid_classement"."raid" TO "etudiants-slam";
GRANT SELECT ON TABLE "raid_classement"."traverser" TO "etudiants-slam";
