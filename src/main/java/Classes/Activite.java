package Classes;

/**
 * The Class Activite.
 */
public class Activite {

	/** The code. */
	private String code;
	
	/** The libelle. */
	private String libelle;
	
	
	
	/**
	 * Instantiates a new activite.
	 */
	public Activite() {
		code = "";
		libelle = "";
	}
	
	/**
	 * Instantiates a new activite.
	 *
	 * @param code the code
	 * @param libelle the libelle
	 */
	public Activite(String code, String libelle) {
	super();
	this.code = code;
	this.libelle = libelle;
	}
	
	
	
	/** 
	 * @return String
	 */
	public String getCode() {
		return code;
	}

	
	/**
	 *  
	 *
	 * @param code the new code
	 */
	public void setCode(String code) {
		this.code = code;
	}
	
	/** 
	 * @return String
	 */
	public String getLibelle() {
		return libelle;
	}
	
	/**
	 *  
	 *
	 * @param libelle the new libelle
	 */
	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}
	
	
}
