package Classes;

import java.time.LocalDate;
import java.util.Date;

/**
 * The Class Coureur.
 */
public class Coureur {

	/** The licence. */
	private String licence;
	
	/** The nom. */
	private String nom;
	
	/** The prenom. */
	private String prenom;
	
	/** The sexe. */
	private String sexe;
	
	/** The nationalite. */
	private String nationalite;
	
	/** The mail. */
	private String mail;
	
	/** The certif med aptitude. */
	private boolean certif_med_aptitude;
	
	/** The datenaiss. */
	private LocalDate datenaiss;
	
	
	
	
	/**
	 * Instantiates a new coureur.
	 */
	public Coureur() {
		licence = "";
		nom = "";
		prenom = "";
		sexe = "";
		nationalite = "";
		mail = "";
		certif_med_aptitude = false;
		datenaiss = LocalDate.of(0, 0, 0);
	}
	
	/**
	 * Instantiates a new coureur.
	 *
	 * @param licence the licence
	 * @param nom the nom
	 * @param prenom the prenom
	 * @param sexe the sexe
	 * @param nationalite the nationalite
	 * @param mail the mail
	 * @param certif_med_aptitude the certif med aptitude
	 * @param datenaiss the datenaiss
	 */
	public Coureur(String licence, String nom, String prenom, String sexe, String nationalite, String mail,
			boolean certif_med_aptitude, LocalDate datenaiss) {
		super();
		this.licence = licence;
		this.nom = nom;
		this.prenom = prenom;
		this.sexe = sexe;
		this.nationalite = nationalite;
		this.mail = mail;
		this.certif_med_aptitude = certif_med_aptitude;
		this.datenaiss = datenaiss;
	}

	/** 
	 * @return String
	 */
	//
	public String getLicence() {
		return licence;
	}
	
	/**
	 *  
	 *
	 * @param licence the new licence
	 */
	public void setLicence(String licence) {
		this.licence = licence;
	}
	
	/** 
	 * @return String
	 */
	public String getNom() {
		return nom;
	}
	
	/**
	 *  
	 *
	 * @param nom the new nom
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}
	
	/** 
	 * @return String
	 */
	public String getPrenom() {
		return prenom;
	}
	
	/**
	 *  
	 *
	 * @param prenom the new prenom
	 */
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	
	/** 
	 * @return char
	 */
	public String getSexe() {
		return sexe;
	}
	
	/**
	 *  
	 *
	 * @param sexe the new sexe
	 */
	public void setSexe(String sexe) {
		this.sexe = sexe;
	}
	
	/** 
	 * @return String
	 */
	public String getNationalite() {
		return nationalite;
	}
	
	/**
	 *  
	 *
	 * @param nationalite the new nationalite
	 */
	public void setNationalite(String nationalite) {
		this.nationalite = nationalite;
	}
	
	/** 
	 * @return String
	 */
	public String getMail() {
		return mail;
	}
	
	/**
	 *  
	 *
	 * @param mail the new mail
	 */
	public void setMail(String mail) {
		this.mail = mail;
	}
	
	/** 
	 * @return boolean
	 */
	public boolean isCertif_med_aptitude() {
		return certif_med_aptitude;
	}
	
	/**
	 *  
	 *
	 * @param certif_med_aptitude the new certif med aptitude
	 */
	public void setCertif_med_aptitude(boolean certif_med_aptitude) {
		this.certif_med_aptitude = certif_med_aptitude;
	}
	
	/** 
	 * @return Date
	 */
	public LocalDate getDatenaiss() {
		return datenaiss;
	}
	
	/**
	 *  
	 *
	 * @param datenaiss the new datenaiss
	 */
	public void setDatenaiss(LocalDate datenaiss) {
		this.datenaiss = datenaiss;
	}
	
	
	
}
