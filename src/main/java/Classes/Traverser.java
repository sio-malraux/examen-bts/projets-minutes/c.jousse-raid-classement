package Classes;
import Classes.Raid;
import Classes.Departement;

/**
 * The Class Traverser.
 */
public class Traverser {

	/** The code raid. */
	Raid code_raid = new Raid();
	
	/** The numero dept. */
	Departement numero_dept = new Departement();
	
	/**
	 * Instantiates a new traverser.
	 */
	public Traverser() {
		code_raid = new Raid();
		numero_dept = new Departement();
	}
	
	/**
	 * Instantiates a new traverser.
	 *
	 * @param code_raid the code raid
	 * @param numero_dept the numero dept
	 */
	public Traverser(Raid code_raid, Departement numero_dept) {
		super();
		this.code_raid = code_raid;
		this.numero_dept = numero_dept;
	}

	/**
	 * Gets the code raid.
	 *
	 * @return the code raid
	 */
	public Raid getCode_raid() {
		return code_raid;
	}

	/**
	 * Sets the code raid.
	 *
	 * @param code_raid the new code raid
	 */
	public void setCode_raid(Raid code_raid) {
		this.code_raid = code_raid;
	}

	/**
	 * Gets the numero dept.
	 *
	 * @return the numero dept
	 */
	public Departement getNumero_dept() {
		return numero_dept;
	}

	/**
	 * Sets the numero dept.
	 *
	 * @param numero_dept the new numero dept
	 */
	public void setNumero_dept(Departement numero_dept) {
		this.numero_dept = numero_dept;
	}
	
	
	
}
