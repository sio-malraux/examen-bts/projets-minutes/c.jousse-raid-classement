package Classes;
import Classes.Coureur;
import Classes.Equipe;

/**
 * The Class Integrer_equipe.
 */
public class Integrer_equipe {

	/** The object equipe. */
	Equipe object_Equipe = new Equipe();
	
	/** The licence. */
	Coureur licence = new Coureur();
	
	/** The num dossard. */
	private int num_dossard;
	
	/**
	 * Instantiates a new integrer equipe.
	 */
	public Integrer_equipe() {
		object_Equipe = new Equipe();
		licence = new Coureur();
		num_dossard = 0;
	}
	
	/**
	 * Instantiates a new integrer equipe.
	 *
	 * @param object_Equipe the object equipe
	 * @param licence the licence
	 * @param num_dossard the num dossard
	 */
	public Integrer_equipe(Equipe object_Equipe, Coureur licence, int num_dossard) {
		super();
		this.object_Equipe = object_Equipe;
		this.licence = licence;
		this.num_dossard = num_dossard;
	}

	/**
	 * Gets the object equipe.
	 *
	 * @return the object equipe
	 */
	public Equipe getObject_Equipe() {
		return object_Equipe;
	}

	/**
	 * Sets the object equipe.
	 *
	 * @param object_Equipe the new object equipe
	 */
	public void setObject_Equipe(Equipe object_Equipe) {
		this.object_Equipe = object_Equipe;
	}

	/**
	 * Gets the licence.
	 *
	 * @return the licence
	 */
	public Coureur getLicence() {
		return licence;
	}

	/**
	 * Sets the licence.
	 *
	 * @param licence the new licence
	 */
	public void setLicence(Coureur licence) {
		this.licence = licence;
	}

	/**
	 * Gets the num dossard.
	 *
	 * @return the num dossard
	 */
	public int getNum_dossard() {
		return num_dossard;
	}

	/**
	 * Sets the num dossard.
	 *
	 * @param num_dossard the new num dossard
	 */
	public void setNum_dossard(int num_dossard) {
		this.num_dossard = num_dossard;
	}
	
	
	
}
