package Classes;

/**
 * The Class Raid.
 *
 * @author Charly
 */
public class Raid {

	/** The code. */
	private String code;
	
	/** The nom. */
	private String nom;
	
	/** The datedebut. */
	private String datedebut;
	
	/** The ville. */
	private String ville;
	
	/** The region. */
	private String region;
	
	/** The nb maxi par equipe. */
	private int nb_maxi_par_equipe;
	
	/** The montant inscription. */
	private double montant_inscription;
	
	/** The nb femmes. */
	private int nb_femmes;
	
	/** The duree maxi. */
	private int duree_maxi; 
	
	/** The age minimum. */
	private int age_minimum;
	
	
	/**
	 * Instantiates a new raid.
	 */
	//Constructor
	public Raid() {
		code = "";
		nom = "";
		datedebut = "";
		ville = "";
		region = "";
		nb_maxi_par_equipe = 0;
		montant_inscription = 0.0;
		nb_femmes = 0;
		duree_maxi = 0;
		age_minimum = 0;
		
		
	}
	
	/**
	 * Instantiates a new raid.
	 *
	 * @param code the code
	 * @param nom the nom
	 * @param datedebut the datedebut
	 * @param ville the ville
	 * @param region the region
	 * @param nb_maxi_par_equipe the nb maxi par equipe
	 * @param montant_inscription the montant inscription
	 * @param nb_femmes the nb femmes
	 * @param duree_maxi the duree maxi
	 * @param age_minimum the age minimum
	 */
	//Constructor with parameters
	public Raid(String code, String nom, String datedebut, String ville, String region, int nb_maxi_par_equipe, double montant_inscription, int nb_femmes, int duree_maxi, int age_minimum) {
		super();
		this.code = code;
		this.nom = nom;
		this.datedebut = datedebut;
		this.ville = ville;
		this.region = region;
		this.nb_maxi_par_equipe = nb_maxi_par_equipe;
		this.montant_inscription = montant_inscription;
		this.nb_femmes = nb_femmes;
		this.duree_maxi = duree_maxi;
		this.age_minimum = age_minimum;
	}
	
	
	
	/** 
	 * @return String
	 */
	//Getter and Setter
	public String getCode() {
		return code;
	}

	
	/**
	 *  
	 *
	 * @param code the new code
	 */
	public void setCode(String code) {
		this.code = code;
	}
	
	/** 
	 * @return String
	 */
	public String getNom() {
		return nom;
	}
	
	/**
	 *  
	 *
	 * @param nom the new nom
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}
	
	/** 
	 * @return String
	 */
	public String getDatedebut() {
		return datedebut;
	}
	
	/**
	 *  
	 *
	 * @param datedebut the new datedebut
	 */
	public void setDatedebut(String datedebut) {
		this.datedebut = datedebut;
	}
	
	/** 
	 * @return String
	 */
	public String getVille() {
		return ville;
	}
	
	/**
	 *  
	 *
	 * @param ville the new ville
	 */
	public void setVille(String ville) {
		this.ville = ville;
	}
	
	/** 
	 * @return String
	 */
	public String getRegion() {
		return region;
	}
	
	/**
	 *  
	 *
	 * @param region the new region
	 */
	public void setRegion(String region) {
		this.region = region;
	}
	
	/** 
	 * @return int
	 */
	public int getNb_maxi_par_equipe() {
		return nb_maxi_par_equipe;
	}
	
	/**
	 *  
	 *
	 * @param nb_maxi_par_equipe the new nb maxi par equipe
	 */
	public void setNb_maxi_par_equipe(int nb_maxi_par_equipe) {
		this.nb_maxi_par_equipe = nb_maxi_par_equipe;
	}
	
	/** 
	 * @return Number
	 */
	public Number getMontant_inscription() {
		return montant_inscription;
	}
	
	/**
	 *  
	 *
	 * @param montant_inscription the new montant inscription
	 */
	public void setMontant_inscription(double montant_inscription) {
		this.montant_inscription = montant_inscription;
	}
	
	/** 
	 * @return int
	 */
	public int getNb_femmes() {
		return nb_femmes;
	}
	
	/**
	 *  
	 *
	 * @param nb_femmes the new nb femmes
	 */
	public void setNb_femmes(int nb_femmes) {
		this.nb_femmes = nb_femmes;
	}
	
	/** 
	 * @return int
	 */
	public int getDuree_maxi() {
		return duree_maxi;
	}
	
	/**
	 *  
	 *
	 * @param duree_maxi the new duree maxi
	 */
	public void setDuree_maxi(int duree_maxi) {
		this.duree_maxi = duree_maxi;
	}
	
	/** 
	 * @return int
	 */
	public int getAge_minimum() {
		return age_minimum;
	}
	
	/**
	 *  
	 *
	 * @param age_minimum the new age minimum
	 */
	public void setAge_minimum(int age_minimum) {
		this.age_minimum = age_minimum;
	}
	
	
	
	
}
