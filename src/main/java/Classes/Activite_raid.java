package Classes;
import Classes.Activite;
import Classes.Raid;

/**
 * The Class Activite_raid.
 */
public class Activite_raid {

	/** The code raid. */
	private String code_raid;
	
	/** The code activite. */
	private String code_activite;
	
	/** The object activite. */
	Activite object_activite = new Activite();
	
	/** The object raid. */
	Raid object_raid = new Raid();
	
	
	/**
	 * Instantiates a new activite raid.
	 */
	//Constructor
	public Activite_raid() {
		code_raid = "";
		code_activite = "";
		object_activite = new Activite();
		object_raid = new Raid();
	}
	
	/**
	 * Instantiates a new activite raid.
	 *
	 * @param code_raid the code raid
	 * @param code_activite the code activite
	 * @param object_activite the object activite
	 * @param object_raid the object raid
	 */
	//Constructor with parameters
	public Activite_raid(String code_raid, String code_activite, Activite object_activite, Raid object_raid) {
		super();
		this.code_raid = code_raid;
		this.code_activite = code_activite;
		this.object_activite = object_activite;
		this.object_raid = object_raid;
	}
	
	
	/** 
	 * @return String
	 */
	//Getter and Setter
	public String getCode_raid() {
		return code_raid;
	}

	
	/**
	 *  
	 *
	 * @param code_raid the new code raid
	 */
	public void setCode_raid(String code_raid) {
		this.code_raid = code_raid;
	}
	
	/** 
	 * @return String
	 */
	public String getCode_activite() {
		return code_activite;
	}
	
	/**
	 *  
	 *
	 * @param code_activite the new code activite
	 */
	public void setCode_activite(String code_activite) {
		this.code_activite = code_activite;
	}
	
	/** 
	 * @return Activite
	 */
	public Activite getObject_activite() {
		return object_activite;
	}
	
	/**
	 *  
	 *
	 * @param object_activite the new object activite
	 */
	public void setObject_activite(Activite object_activite) {
		this.object_activite = object_activite;
	}
	
	/** 
	 * @return Raid
	 */
	public Raid getObject_raid() {
		return object_raid;
	}
	
	/**
	 *  
	 *
	 * @param object_raid the new object raid
	 */
	public void setObject_raid(Raid object_raid) {
		this.object_raid = object_raid;
	}
	
	
	
}
