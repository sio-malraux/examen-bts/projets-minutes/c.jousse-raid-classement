package Classes;

/**
 * The Class Departement.
 */
public class Departement {

	/** The numero. */
	private int numero;
	
	/** The nom. */
	private String nom;
	
	
	/**
	 * Instantiates a new departement.
	 */
	public Departement() {
		numero = 0;
		nom = "";
	}
	
	/**
	 * Instantiates a new departement.
	 *
	 * @param numero the numero
	 * @param nom the nom
	 */
	public Departement(int numero, String nom) {
		super();
		this.numero = numero;
		this.nom = nom;
	}

	/**
	 * Gets the numero.
	 *
	 * @return the numero
	 */
	public int getNumero() {
		return numero;
	}

	/**
	 * Sets the numero.
	 *
	 * @param numero the new numero
	 */
	public void setNumero(int numero) {
		this.numero = numero;
	}

	/**
	 * Gets the nom.
	 *
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * Sets the nom.
	 *
	 * @param nom the new nom
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}
	
	
}
