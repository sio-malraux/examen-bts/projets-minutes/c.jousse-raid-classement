package Classes;
import java.time.LocalDate;

import Classes.Raid;

/**
 * The Class Equipe.
 */
public class Equipe {

	/** The numero. */
	private int numero;
	
	/** The code raid. */
	Raid code_raid = new Raid();
	
	/** The nom. */
	private String nom;
	
	/** The dateinscription. */
	private LocalDate dateinscription;
	
	/** The temps global. */
	private int temps_global;
	
	/** The classement. */
	private int classement;
	
	/** The penalites bonif. */
	private int penalites_bonif;
	
	/**
	 * Instantiates a new equipe.
	 */
	public Equipe() {
		numero = 0;
		code_raid = new Raid();
		nom = "";
		dateinscription = LocalDate.of(0, 0, 0);
		temps_global = 0;
		classement = 0;
		penalites_bonif = 0;
	}
	
	/**
	 * Instantiates a new equipe.
	 *
	 * @param numero the numero
	 * @param code_raid the code raid
	 * @param nom the nom
	 * @param dateinscription the dateinscription
	 * @param temps_global the temps global
	 * @param classement the classement
	 * @param penalites_bonif the penalites bonif
	 */
	public Equipe(int numero, Raid code_raid, String nom, LocalDate dateinscription, int temps_global, int classement,
			int penalites_bonif) {
		super();
		this.numero = numero;
		this.code_raid = code_raid;
		this.nom = nom;
		this.dateinscription = dateinscription;
		this.temps_global = temps_global;
		this.classement = classement;
		this.penalites_bonif = penalites_bonif;
	}

	/**
	 * Gets the numero.
	 *
	 * @return the numero
	 */
	public int getNumero() {
		return numero;
	}

	/**
	 * Sets the numero.
	 *
	 * @param numero the new numero
	 */
	public void setNumero(int numero) {
		this.numero = numero;
	}

	/**
	 * Gets the code raid.
	 *
	 * @return the code raid
	 */
	public Raid getCode_raid() {
		return code_raid;
	}

	/**
	 * Sets the code raid.
	 *
	 * @param code_raid the new code raid
	 */
	public void setCode_raid(Raid code_raid) {
		this.code_raid = code_raid;
	}

	/**
	 * Gets the nom.
	 *
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * Sets the nom.
	 *
	 * @param nom the new nom
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}

	/**
	 * Gets the dateinscription.
	 *
	 * @return the dateinscription
	 */
	public LocalDate getDateinscription() {
		return dateinscription;
	}

	/**
	 * Sets the dateinscription.
	 *
	 * @param dateinscription the new dateinscription
	 */
	public void setDateinscription(LocalDate dateinscription) {
		this.dateinscription = dateinscription;
	}

	/**
	 * Gets the temps global.
	 *
	 * @return the temps global
	 */
	public int getTemps_global() {
		return temps_global;
	}

	/**
	 * Sets the temps global.
	 *
	 * @param temps_global the new temps global
	 */
	public void setTemps_global(int temps_global) {
		this.temps_global = temps_global;
	}

	/**
	 * Gets the classement.
	 *
	 * @return the classement
	 */
	public int getClassement() {
		return classement;
	}

	/**
	 * Sets the classement.
	 *
	 * @param classement the new classement
	 */
	public void setClassement(int classement) {
		this.classement = classement;
	}

	/**
	 * Gets the penalites bonif.
	 *
	 * @return the penalites bonif
	 */
	public int getPenalites_bonif() {
		return penalites_bonif;
	}

	/**
	 * Sets the penalites bonif.
	 *
	 * @param penalites_bonif the new penalites bonif
	 */
	public void setPenalites_bonif(int penalites_bonif) {
		this.penalites_bonif = penalites_bonif;
	}
	
	
	
}
